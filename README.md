# SbTeX

The Stratish typesetter.

[discord.gg/X5SuQTb](https://discord.gg/X5SuQTb)


## Usage

### Command-line

    ./main.py words to typeset

### Library

    import sbtex
	
	words = ["words", "to", "typeset"]
    
    # single-use
    sbtex.sbtex(words) -> bytes
    
    # if you intend to do multiple
    typesetter = sbtex.SbTypeset()
    typesetter(words) -> bytes
	typesetter.img(words) -> wand.Image

## Run Time

It appears to typeset *n* characters in *O(n)* time. On my machine it takes about 8.5ms per character. I hope to get this down.
