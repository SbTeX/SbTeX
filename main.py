#!/usr/bin/env python3

# Command-line for sbtex

import os
import sys

import argparse
from wand.display import display
import logging

import sbtex


def main():
	ns = parse()
	logging.basicConfig(level=ns.log)
	img = sbtex.img(ns.words,
		fontpath = ns.fontpath
	)
	if(ns.display):
		display(img)
	ns.outfile.write(img.make_blob())

def parse():
	parser = argparse.ArgumentParser(
		description="sbtex - The Stratish typesetter.",
	)
	
	parser.add_argument(
		"-V",
		help="Print version and exit",
		action="version",
		version=sbtex.version
	)
	
	parser.add_argument(
		"-q",
		help="Less output",
		action="count",
		default=0
	)
	
	parser.add_argument(
		"-v",
		help="More output",
		action="count",
		default=0
	)
	
	
	parser.add_argument(
		"--display",
		help="display image on-screen when done",
		action="store_true"
	)
	
	parser.add_argument(
		"-o",
		help="Write to OUTFILE instead of sbtex.out.png",
		type=argparse.FileType('wb'),
		default="sbtex.out",
		dest="outfile"
	)
	parser.add_argument(
		"-f",
		help="Use FONT instead of sans.",
		metavar="FONT",
		dest="font",
		default="sans",
		type=str,
	)
	parser.add_argument(
		"-F",
		help="Add DIR to font include path.",
		action="append",
		metavar="DIR",
		dest="include",
		default=["/etc/sbtex/fonts/"]
	)
	
	parser.add_argument(
		"words",
		metavar = "WORD",
		nargs = "*"
	)
	
	ns = parser.parse_args()
	
	raw_log = ns.v - ns.q
	del ns.v, ns.q
	
	if(raw_log == 0): # no verbose or quiet
		ns.log = logging.INFO
	if(raw_log > 0): # -v
		ns.log = logging.DEBUG - (raw_log-1)
	if(raw_log < 0): # -q
		ns.log = logging.INFO - raw_log*10
	if(ns.log < 1): ns.log = 1
	
	
	if(ns.outfile == sys.stdout): ns.outfile = sys.stdout.buffer
	
	ns.fontpath = fontpath(ns.include, ns.font)
	
	return ns

def fontpath(include, name):
	for d in include:
		path = os.path.join(d, name)
		if(os.path.isdir(path)):
			return path
	
	print("{}: font not found.".format(name), file=sys.stderr)
	exit(1)

if(__name__ == "__main__"):
	main()
