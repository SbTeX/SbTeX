from setuptools import setup

from sbtex import __version__

setup(
	name="sbtex",
	version=__version__,
	description="The Stratish typesetter",
	url="https://gitlab.com/SbTeX/",
	author="Connor \"Blacksilver\" [REDACTED]",
	author_email="blacksilverck35@gmail.com",
	license="GPL",
	packages=["sbtex"]
)
