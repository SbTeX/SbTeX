#!/usr/bin/env python3

# SbTeX benchmarker
import subprocess
import time
import logging

import sbtex

logging.basicConfig(level=logging.WARNING)

n_tests = 10

all_results = []
typesetter = sbtex.SbTypeset()

typesetter("asdf") # the first time takes longer, so get rid of that outlier.

def bench(*words):
	n_glyph = sum(len(w) for w in words)
	total = 0
	before = time.time()
	
	for i in range(n_tests):
		typesetter(words)
	
	after = time.time()
	total += (after - before)
	
	avg = total/n_tests
	time_per_glyph = avg / n_glyph
	ms_per_glyph = time_per_glyph * 1000
	print("{:05.2f}ms/glyph".format(ms_per_glyph))
	all_results.append(ms_per_glyph)


bench("xyzzy")
bench("hello","world")
bench("this","was","a","triumph")
bench("abacabadabacaba")
bench("aeiouyaeiouyaeiouyaei")
bench("bcdfghjklmnpqrstuvwxz")
bench("a","lot","of","words","to","test","how","it","handles","that")
bench("asinglereallylongwordletsseehowyoulikethismate")
bench("more", "random", "tests")
bench("content")
bench("for","science","you","monster")
bench("the", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog")

print()
print("Average: {:.2f}ms/glyph".format(sum(all_results)/len(all_results)))
