from collections import namedtuple

class Size:
	"""The size of a thing."""
	def __init__(self,x,y):
		self.x = x
		self.y = y
	def __repr__(self):
		return str((self.x, self.y))

GLYPH   = Size(100,100)
DECO    = Size(100,20)
MARGIN  = Size(20,20)
PADDING = Size(10,10)

def calculate_size(words):
	"""Calculate the max size of several words"""
	siz = Size(0, 0)
	
	for word in words:
		siz.x += word.x()
		siz.y = max(siz.y, word.y())
	
	siz.x += PADDING.x * (len(words)-1)
	
	siz.x += MARGIN.x*2
	siz.y += MARGIN.y*2
	
	return siz
