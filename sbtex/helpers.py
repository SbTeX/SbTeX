def vowel(c):
	"""Return True if it's one of [aeiou], else False."""
	return (c.lower() in ["a","e","i","o","u","y"])
