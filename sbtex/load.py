import os

from wand.image import Image

class MetaImage:
	"""Stores an Image and the assosiated char"""
	def __init__(self, ns, image, char):
		self.image = image
		self.char = char.lower()
		
		size = image.size
		small_x, small_y = (image.width//2, image.height//2)
		
		self.small = image.clone()
		if(ns.resize_method == "sample"):
			self.small.sample(small_x, small_y)
		else:
			self.small.resize(small_x, small_y, filter=ns.resize_method)
	
	def __repr__(this):
		return f"MetaImage({this.char})"

def fail_glyph(ns, char):
	"""Return the MetaImage for /etc/sbtex/fonts/_fail.png"""
	return MetaImage(ns,
		Image(
			filename = "/etc/sbtex/fonts/_fail.png"
		), char
	)
def fail_deco(ns, char):
	"""Return the MetaImage for /etc/sbtex/fonts/_decofail.png"""
	return MetaImage(ns,
		Image(
			filename = "/etc/sbtex/fonts/_decofail.png"
		), char
	)

def load_deco(ns, c):
	"""Load the given char as a decoration"""
	path = os.path.join(ns.fontpath, "decos/{}.png".format(c.upper()))
	if(os.path.exists(path)):
		return MetaImage(ns,
			Image(
				filename = path
			), c
		)
	else:
		return fail_deco(ns, c)

def load_glyph(ns, c):
	"""Load the given char as a glyph"""
	path = os.path.join(ns.fontpath, "glyphs/{}.png".format(c.upper()))
	if(os.path.exists(path)):
		return MetaImage(ns,
			Image(
				filename = path
			), c
		)
	else:
		return fail_glyph(ns, c)

