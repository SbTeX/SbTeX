if(__name__ == "__main__"):
	from size import GLYPH
else:
	from .size import GLYPH

lft = GLYPH.x//8
ctr = GLYPH.x//4
rht =(GLYPH.x//8)*3

top = GLYPH.y//8
mid = GLYPH.y//4
dwn =(GLYPH.y//8)*3


def offset(pos):
	"""
		Where should an inner glyph be, based on the position from manifest.json?
		
		Returns (left, top) for pos in [1,9], and None otherwise.
	"""
	if  (pos == 1):
		return (lft, top)
	elif(pos == 2):
		return (ctr, top)
	elif(pos == 3):
		return (rht, top)
	elif(pos == 4):
		return (lft, mid)
	elif(pos == 5):
		return (ctr, mid)
	elif(pos == 6):
		return (rht, mid)
	elif(pos == 7):
		return (lft, dwn)
	elif(pos == 8):
		return (ctr, dwn)
	elif(pos == 9):
		return (rht, dwn)

if(__name__ == "__main__"):
	# Debug script, don't mind me...
	print("top:", top)
	print("mid:", mid)
	print("dwn:", dwn)
	print("lft:", lft)
	print("ctr:", ctr)
	print("rht:", rht)
	print()
	for i in range(1,9):
		print("offset({}) = {}".format(i, offset(i)))
