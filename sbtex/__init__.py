"""SbTeX - The Stratish typesetter.

Exports:
	SbTypesetError -- Misc. Typesetting error (TODO: More specific error classes)
	SbTypeset -- Reusable, callable, class which does the dirty work.
	sbtex -- Typeset and return the image data.
	blob -- Typeset and return the image data.
	img -- Typeset and return the Wand Image Object.
"""

import os
import sys
import logging
import json

import wand
from wand.image import Image
from wand.color import Color
from wand.drawing import Drawing

from .word import Word
from .size import calculate_size
from .size import MARGIN, PADDING
from .load import load_glyph, load_deco
from .version import version_info, version, __version__

class SbTypesetError(Exception):
	"""Generic typeset error."""
	pass

class SbTypeset:
	"""
		The main SbTeX class - holds all of you settings and stuff.

		Initialize:
			typesetter = SbTypeset() # see doc for __init__
			
		Invocation:
			typesetter(["typeset", "these", "words"])      -> Bytes
			typesetter.blob(["typeset", "these", "words"]) -> Bytes
			typesetter.img(["typeset", "these", "words"])  -> wand.image.Image
	"""
	def __init__(self,
		fontpath="/etc/sbtex/fonts/sans/",
		logger=logging.getLogger(__name__),
		resize_method = "sample",
		fmt="png",
		strip_punc = False
	):
		"""
			Initialize the class.
			
			Optional arguments:
				fontpath      - directory where all the glyphs are stored [/etc/sbtex/fonts/sans/]
				logger        - logging object to use                     [logging.getLogger(__name__)]
				resize_method - Wand method to use when resizing images   [sample]
				fmt           - output format                             [png]
				strip_punc    - strip punctuation before typesetting      [False]
			
			TODO:
			  * preload       - pre-load all images into memory           [0]
			  * filler_glyph  - use the newly-discovered "filler glyph"   [True]
		"""
		self.fontpath = fontpath
		self.log = logger
		self.resize_method = resize_method
		self.fmt = fmt
		self.strip_punc = strip_punc
		
		self.manifest_path = os.path.join(fontpath, "manifest.json")
		with open(self.manifest_path, "r") as f:
			self.manifest = json.load(f)
	
	def _typeset(self, words):
		"""Internal, DNU."""
		nglyph = 0
		for i, word in enumerate(words):
			if(self.strip_punc):
				words[i] = "".join([c for c in words[i] if c.isalpha()])
			nglyph += len(word)
		
		if(nglyph==0):
			raise SbTypesetError("Refusing to parse empty input.")
			exit(1)
		
		Words = [Word(self, i) for i in words]
		size = calculate_size(Words)
		
		self.log.info("Image size: %dx%d", size.x, size.y)
		
		img = Image(
			width=size.x,
			height=size.y,
			background=Color("white")
		)
		img.format=self.fmt
		x = MARGIN.x
		for w in Words:
			self.log.log(logging.DEBUG, "Word \"{}\": {}".format(w, w.size()))
			w.set(img, x, MARGIN.y)
			x += w.x() + PADDING.x
			
		return img
	
	def __call__(self, words):
		"""Actually typeset the sole argument, a list of strings. Return the image's data as Bytes."""
		return self.blob(words)
		
	def blob(self, words):
		"""Actually typeset the sole argument, a list of strings. Return the image's data as Bytes."""
		return self.img(words).make_blob()
		
	def img(self, words):
		"""Actually typeset the sole argument, a list of strings. Return the Wand Image object."""
		return self._typeset(words)

def sbtex(words, **kwargs):
	return SbTypeset(**kwargs)(words)
def blob(words, **kwargs):
	return SbTypeset(**kwargs).blob(words)
def img(words, **kwargs):
	return SbTypeset(**kwargs).img(words)

# vim: ts=4
