from collections import namedtuple

class VersionInfo:
	def __init__(self, major, minor, patch, level=None, build=None, codename=None):
		self.major = major
		self.minor = minor
		self.patch = patch
		self.level = level
		self.build = build
		self.codename = codename
	
	def __call__(self, codename=True):
		fmt = "{major}.{minor}.{patch}"
		if(self.level):
			fmt = fmt + "-{level}"
		if(self.build):
			fmt = fmt + "+{build}"
		if(self.codename and codename):
			fmt = fmt + " ({codename})"
		
		return fmt.format(
			major=self.major,
			minor=self.minor,
			patch=self.patch,
			level=self.level,
			build=self.build,
			codename=self.codename
		)
	def __repr__(self):
		return self(codename=False)
	def __str__(self):
		return self(codename=True)
	

version_info = VersionInfo(0,2,1, codename="The Imported")
version = str(version_info)
__version__ = repr(version_info)
