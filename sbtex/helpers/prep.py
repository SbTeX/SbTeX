import os, sys
from errors import FontError


def fontpath(include_dirs, name):
	if(include_dirs == None): include_dirs = ["/etc/sbtex/fonts/"]
	for d in include_dirs:
		path = os.path.join(d, name)
		if(os.path.isdir(path)):
			return path
	
	raise FontError("%s: font not found" % name)
