import sys, os

import logging

from wand.image import Image

from .helpers import vowel
from .size import GLYPH, DECO, PADDING
from .set import set_large, set_small, set_deco
from .load import load_glyph, load_deco


class Word:
	"""Describes a word."""
	def __init__(self, ns, word):
		self.word = word
		self.ns = ns
		
		self.vowel_glyph = []
		self.consn_glyph = []
		self.vowel_deco = []
		self.consn_deco = []
		
		for c in word:
			if(vowel(c)):
				if(len(self.vowel_glyph) < 2):
					self.vowel_glyph.append(load_glyph(ns, c))
				else:
					self.vowel_deco.append(load_deco(ns, c))
			else:
				if(len(self.consn_glyph) < 2):
					self.consn_glyph.append(load_glyph(ns, c))
				else:
					self.consn_deco.append(load_deco(ns, c))
		
		ns.log.log(logging.DEBUG-3, "\"%s\": %s %s %s %s", self.word, self.vowel_glyph, self.consn_glyph, self.vowel_deco, self.consn_deco)
	
	def __str__(self):
		return self.word
	
	def vowel_x(self):
		return GLYPH.x if self.vowel_glyph else 0
	
	def consn_x(self):
		return GLYPH.x if self.consn_glyph else 0
	
	def deco_y(self):
		cy = 0
		vy = 0
		if(self.consn_deco):
			for i in self.consn_deco:
				cy+=i.image.height
				cy+=PADDING.y
		if(self.vowel_deco):
			for i in self.vowel_deco:
				vy+=i.image.height
				vy+=PADDING.y
		return max(cy,vy)

	def x(self):
		vowel_x = self.vowel_x()
		consn_x = self.consn_x()
		pad = PADDING.x if (vowel_x and consn_x) else 0
		return self.vowel_x() + self.consn_x() + pad
	
	def y(self):
		return GLYPH.y + self.deco_y()
	
	def size(self):
		return (self.x(), self.y())
	
	def set(self, img, x0, y0):
		"""Set the entire word on img, with the top-left of the first glyph at (x0,y0)"""
		
		x = x0
		
		# Set consanant glyphs
		if(len(self.consn_glyph) > 0):
			child = None
			if(len(self.consn_glyph) > 1):
				child = self.consn_glyph[1]
			
			set_rtn = set_large(self.ns, img, self.consn_glyph[0], x, y0, child=child)
			
			if(set_rtn != True):
				self.consn_deco.insert(0, load_deco(self.ns, child.char))
			
			# Set consanant decos
			y = y0 + GLYPH.y + PADDING.y
			for i, c in enumerate(self.consn_deco):
				set_deco(self.ns, img, c, x, y)
				y += c.image.height + PADDING.y
			
			x += GLYPH.x + PADDING.x
			
		
		# Set vowel glyphs
		if(len(self.vowel_glyph) > 0):
			child = None
			if(len(self.vowel_glyph) > 1):
				child = self.vowel_glyph[1]
			
			set_rtn = set_large(self.ns, img, self.vowel_glyph[0], x, y0, child=child)
			
			if(set_rtn != True):
				self.vowel_deco.insert(0, load_deco(self.ns, child.char))
			
		
			# Set vowel decos
			y = y0 + GLYPH.y + PADDING.y
			for i, c in enumerate(self.vowel_deco):
				set_deco(self.ns, img, c, x, y)
				y += c.image.height + PADDING.y
		
		return img
