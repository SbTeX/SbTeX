import os

import logging

from wand.image import Image
from .size import GLYPH
from .offset import offset

def set_large(ns, dest, image, left, top, child=None):
	"""Place a large glyph at (left, top), calling set_small if child!=None

	Return value:
		True if OK.
		child if it can't be typeset inside this glyph (inner_pos = 0)
	"""
	ns.log.log(logging.DEBUG-1, "  Large glyph '{char}' at ({left},{top})".format(char=image.char,left=left,top=top))
	
	dest.composite(
		image = image.image,
		left = left,
		top = top
	)
	if(child):
		offset_pos = offset(ns.manifest["inner_pos"].get(image.char, 5))
		
		if(offset_pos == None):
			return child
		
		dx, dy = offset_pos
		set_small(ns, dest, child, image, left+dx, top+dy)
	
	return True

def set_small(ns, dest, image, parent, left, top):
	"""Typeset a small glyph at (left, top)"""
	ns.log.log(logging.DEBUG-1,"  Small glyph '{char}' at ({left},{top})".format(char=image.char,left=left,top=top))
	
	kerning = ns.manifest.get("kerning", {})
	parent_kerning = kerning.get(parent.char, {})
	rotation = parent_kerning.get(image.char, 0)
	
	with image.small.clone() as rotated:
		rotated.rotate(rotation)
		dest.composite(
			image = rotated,
			left = left,
			top = top
		)

def set_deco(ns, dest, image, left, top):
	"""Typeset a deco at (left, top)"""
	ns.log.log(logging.DEBUG-1, "  Decoration  '{char}' at ({left},{top})".format(char=image.char,left=left,top=top))
	
	dest.composite(
		image = image.image,
		left = left,
		top = top
	)
